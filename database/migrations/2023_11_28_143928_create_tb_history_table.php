<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_history', function (Blueprint $table) {
            $table->id('ID_History');
            $table->string('Status_Kegiatan', 30)->nullable();
            $table->date('Tanggal_Kegiatan')->nullable();
            $table->timestamps();
            $table->bigInteger('ID_Kegiatan')->unsigned();
            $table->char('NIM', 15);

            // Foreign key constraint
            $table->foreign('ID_Kegiatan')->references('ID_Kegiatan')->on('tb_kegiatan')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_history');
    }
};
