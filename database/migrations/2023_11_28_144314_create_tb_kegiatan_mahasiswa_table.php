<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_kegiatan_mahasiswa', function (Blueprint $table) {
            $table->char('NIM', 15);
            $table->bigIncrements('id_kegiatan');

            // Define primary key
            $table->primary('id_kegiatan');

            // Define foreign keys
            $table->foreign('NIM')->references('NIM')->on('users')->onDelete('CASCADE');
            $table->foreign('id_kegiatan')->references('ID_Kegiatan')->on('tb_kegiatan')->onDelete('CASCADE')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_kegiatan_mahasiswa');
    }
};
