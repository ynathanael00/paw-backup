<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_kegiatan', function (Blueprint $table) {
            $table->bigIncrements('ID_Kegiatan');
            $table->string('Kategori_Kegiatan', 30)->nullable();
            $table->string('Nama_Kegiatan', 50)->nullable();
            $table->string('Foto_Kegiatan', 100)->nullable();
            $table->date('Tanggal_Kegiatan')->nullable();
            $table->string('Link_Kegiatan', 100)->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_kegiatan');
    }
};
