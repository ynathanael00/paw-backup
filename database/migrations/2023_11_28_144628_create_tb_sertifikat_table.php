<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_sertifikat', function (Blueprint $table) {
            $table->bigIncrements('ID_Sertifikat');
            $table->string('Nama_Kegiatan', 45)->nullable();
            $table->text('Deskripsi_Sertifikat')->nullable();
            $table->date('Tanggal_Kegiatan')->nullable();
            $table->string('Gambar_Sertifikat', 200)->nullable();
            $table->char('NIM', 15);
            $table->char('id_kegiatan', 5);

            $table->foreign('NIM')->references('NIM')->on('users');
            $table->foreign('id_kegiatan')->references('ID_Kegiatan')->on('tb_kegiatan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_sertifikat');
    }
};
