<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'NIM' => '225150700111001',
                'Nama_User' => 'Ikram Sabila',
                'email' => '225150700111001',
                'Fakultas' => 'Fakultas Ilmu Komputer',
                'Program_Studi' => 'Teknologi Informasi',
                // Add other fields as needed
                'password' => '$2y$10$d7WNe/PcV1yPm6i/ZzV.vOc8ubi9ztLwafy9QW4YB/j.mxik/U.EK',
                'created_at' => '2023-11-26 20:28:15',
                'updated_at' => '2023-11-26 20:28:15',
            ],
            [
                'NIM' => '225150700111002',
                'Nama_User' => 'Sandro Christopher Sibuea',
                'email' => '225150700111002',
                'Fakultas' => 'Fakultas Teknik Pertanian',
                'Program_Studi' => 'Bioteknologi',
                // Add other fields as needed
                'password' => '$2y$10$Uq0YjdtiGjFgZuS3p52u8Ou.LEIKyq/1A..8yruBl7Zc10mXV3O86',
                'created_at' => '2023-11-26 20:30:04',
                'updated_at' => '2023-11-26 20:30:04',
            ],
            // Add other data entries here
        ];

        DB::table('test_users')->insert($data);
    }
}
