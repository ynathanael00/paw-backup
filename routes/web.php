<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\HistoryController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\certController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AddArticleController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\NamaController;
use App\Http\Controllers\ProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/login', [LoginController::class, 'authenticate']);
Route::get('/login', [LoginController::class, 'index'])->middleware('guest')->name('login');
Route::post('/logout', [LoginController::class, 'logout'])  ;
// Route::get('/login', [LoginController::class, 'index']); 

Route::get('/home', function() {
    return view('home.index')->with('title', 'Home');
})->name('home')->middleware('auth');

//landingPage

Route::get('/', function () {
    return view('landingpage.landingPage')->with('title', 'Welcome');
})->middleware('guest');


// history

Route::get('/history', [HistoryController::class, 'show'])->name('history')->middleware('auth');
Route::get('/history/all', [HistoryController::class, 'index'])->name('history')->middleware('auth');

Route::get('/history/{id_history}', [HistoryController::class, 'edit'])->name('History.edit')->middleware('auth');

//sertifikat

Route::get('/sertifikat', [certController::class, 'getUserCert'])->name('sertifikat')->middleware('auth');


//home

Route::get('/home', [HomeController::class, 'index'])->name('index')->middleware('auth');

Route::get('/article/{ID_Kegiatan}', [ArticleController::class, 'show'])->name('article.show')->middleware('auth');

Route::get('/add-article', [AddArticleController::class, 'create'])->name('article.create')->middleware('auth');
Route::post('/store-article', [AddArticleController::class, 'store'])->name('article.store')->middleware('auth');

Route::get('/edit-article/{ID_Kegiatan}', [ArticleController::class, 'edit'])->name('article.edit')->middleware('auth');
Route::put('/update-article/{ID_Kegiatan}', [ArticleController::class, 'update'])->name('article.update')->middleware('auth');

Route::delete('/delete-article/{ID_Kegiatan}', [ArticleController::class, 'destroy'])->name('article.destroy')->middleware('auth');


//profile

Route::get('/profiles', [NamaController::class, 'show'])->name('profiles.show')->middleware('auth');

Route::post('/update-profile', [ProfileController::class, 'update'])->name('update-profile');

