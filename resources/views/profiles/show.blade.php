<!-- resources/views/profiles/show.blade.php -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User Profile</title>
    <link rel="stylesheet" href="{{ asset('css/profile.css') }}">
</head>
<body>
    <div class="container">
        <!-- Top Circular Image Container -->
        <div class="image-container">
            <!-- Add a link or use Blade syntax to display the image -->
            <img src="https://images.rawpixel.com/image_png_800/cHJpdmF0ZS9sci9pbWFnZXMvd2Vic2l0ZS8yMDIzLTAxL3JtNjA5LXNvbGlkaWNvbi13LTAwMi1wLnBuZw.png" alt="Profile Image">
        </div>

        <!-- Profile Information -->
        <div class="profile-info">
            <div class="heading">Profile</div>
            <div class="user-name">{{ $profile->Nama_User }}</div>
            <div class="nim">{{ $profile->NIM }}</div>
        </div>

        <!-- Sidebar Features -->
        <div class="sidebar">
            <div class="feature">History</div>
            <hr>
            <div class="feature">Certificate</div>
            <hr>
            <div class="feature">Settings</div>
            <hr>
            <div class="feature">Log Out</div>
        </div>
    </div>
</body>
</html>
