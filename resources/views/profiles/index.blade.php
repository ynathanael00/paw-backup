@extends('layouts.main')
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
</head>


@section('container')

    <body>
        @isset($message)
        <div class="alert alert-success">
            <p>Profile Updated Successfully</p>
        </div>
        @endisset
        
        <div class="container m-auto" style="height: 100%;">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <div class="card">
                        <div class="card-header" style="display: flex; align-items:center;">
                            <h3 style="align-self: end;">User Profile</h3>
                            <img src="{{ auth()->user()->Foto_Profile }}" alt="" width="50px"
                                class="ms-auto" style="border-radius: 50px;">
                        </div>
                        <div class="card-body">
                            <ul class="list-group">
                                <strong class="">Name:</strong>
                                <li class="list-group-item" style="min-height: 2.6em;">
                                    <span class="py-1" id="nameField" contenteditable="false" style="">{{ auth()->user()->Nama_User }}</span>
                                </li>
                                <strong class="mt-4">NIM:</strong>
                                <li class="list-group-item" style="min-height: 2.6em;">
                                    <span id="emailField" contenteditable="false">{{ auth()->user()->NIM }}</span>
                                </li>
                                <strong class="mt-4">Fakultas:</strong>
                                <li class="list-group-item" style="min-height: 2.6em;">
                                    <span id="fakultasField" contenteditable="false">{{ auth()->user()->Fakultas }}</span>
                                </li>
                                <strong class="mt-4">Program Studi:</strong>
                                <li class="list-group-item" style="min-height: 2.6em;">
                                    <span id="prodiField" contenteditable="false">{{ auth()->user()->Program_Studi }}</span>
                                </li>
                                <div class="mt-3" style="display: flex;">
                                    <button type="button" id="editBtn" class="btn btn-danger" style="width: -webkit-fill-available;">Edit</button>
                                    <button type="submit" id="saveBtn" class="btn btn-primary" style="display: none; width: -webkit-fill-available;">Save Profile</button>
                                    <button type="button" id="refresh" class="btn btn-secondary mx-2"><i class="bi bi-arrow-counterclockwise"></i></button>
                                </div>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

        <script>
            document.getElementById('editBtn').addEventListener('click', function() {
                // Enable editing for fields
                document.getElementById('nameField').setAttribute('contenteditable', 'true');
                document.getElementById('fakultasField').setAttribute('contenteditable', 'true');
                document.getElementById('prodiField').setAttribute('contenteditable', 'true');

                // Toggle button visibility
                document.getElementById('editBtn').style.display = 'none';
                document.getElementById('saveBtn').style.display = '';
            });

            document.getElementById('saveBtn').addEventListener('click', function() {
                const updatedData = {
                    nama: document.getElementById('nameField').innerText,
                    nim: document.getElementById('emailField').innerText,
                    fakultas: document.getElementById('fakultasField').innerText,
                    program_studi: document.getElementById('prodiField').innerText,
                };

                // Disable editing and toggle button visibility
                disableEditing();

                $.ajax({
                    url: "{{ route('update-profile') }}",
                    type: "POST",
                    data: updatedData, // Send the updated data
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    success: function(response) {
                        console.log(response.message);
                        // Optionally update UI to show success message or handle accordingly
                    },
                    error: function(xhr) {
                        console.error('Error:', xhr.responseText);
                        // Handle error scenarios
                    }
                });
            });


            function disableEditing() {
                document.getElementById('nameField').setAttribute('contenteditable', 'false');
                document.getElementById('fakultasField').setAttribute('contenteditable', 'false');
                document.getElementById('prodiField').setAttribute('contenteditable', 'false');

                document.getElementById('editBtn').style.display = 'block';
                document.getElementById('saveBtn').style.display = 'none';
            }

            document.body.addEventListener('keypress', function(event) {
                if (event.key === 'Enter') {
                    // Check if the save button is visible to ensure it triggers only when in edit mode
                    if (document.getElementById('saveBtn').style.display === '') {
                        // Perform save action when Enter is pressed
                        document.getElementById('saveBtn').click();
                    }
                }
            });

            document.getElementById('refresh').addEventListener('click', function() {
                // Enable editing for fields
                location.reload();
            });
        </script>
    </body>
@endsection

</html>
