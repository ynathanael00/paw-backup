<html lang="id">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="{{ asset('css/addArticle.css') }}">

</head>

<body>
    <section class="tambah">
        <form action="{{ route('article.store') }}" method="POST">
            @csrf
            <h1>Tambah Artikel</h1>
            <p>Nama Kegiatan</p>
            <input type="text" name="Nama_Kegiatan"><br>
            <p>Kategori Kegiatan</p>
            <input type="text" name="Kategori_Kegiatan"><br>
            <p>Foto Kegiatan</p>
            <input type="text" name="Foto_Kegiatan"><br>
            <p>Tanggal</p>
            <input type="date" name="Tanggal_Kegiatan"><br>
            <p>Link Pendaftaran</p>
            <input type="text" name="Link_Kegiatan"><br>

            <button type="submit">Add</button>
        </form>
    </section>
</body>

</html>
