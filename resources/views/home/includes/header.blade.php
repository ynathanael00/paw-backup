<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Your Website Title</title>
    <!-- Tambahkan link ke file CSS dan JavaScript jika diperlukan -->
    <link rel="stylesheet" href="{{ asset('css/header.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" />

</head>

<body>
    <header>
            <div class="header-wrapper">
                <img src="https://drive.google.com/uc?id=1zFxk4DNeNhtL-WZ9j-lWVzilC45Gp6Ma" alt="Logo Brawevent"
                    class="logo">
                <nav>
                    <a href="#" class="nav-link">Home</a>
                    <a href="#" class="nav-link">History</a>
                    <a href="#" class="nav-link">Certificate</a>
                </nav>
            </div>
    </header>
</body>

</html>
