@extends('layouts.main')
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Article Page</title>
    <link rel="stylesheet" href="{{ asset('css/article.css') }}">
    @section('container')

    <body>
        <div class="container">
            <div class="desc">
                <img class="image" src="{{ $kegiatan->Foto_Kegiatan }}" />
                <div class="penjelasan">

                    <div class="section-title">
                        <h3>{{ $kegiatan->Nama_Kegiatan }}</h3>
                    </div>
                    <p class="description">
                        id kegiatan : {{ $kegiatan->ID_Kegiatan }} <br>
                        Kategori Kegiatan :{{ $kegiatan->Kategori_Kegiatan }} <br>
                        Nama Kegiatan : {{ $kegiatan->Nama_Kegiatan }} <br>
                        Tanggal Kegiatan : {{ $kegiatan->Tanggal_Kegiatan }} <br>
                        Link :{{ $kegiatan->Link_Kegiatan }}
                    </p>
                    <p class="text-wrapper">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet felis vitae purus tincidunt
                        euismod.
                        Nullam euismod tortor sed lectus facilisis, ac suscipit urna consectetur. Praesent eget turpis nec
                        justo
                        varius
                        malesuada
                    </p>
                    <p class="div">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet felis vitae purus tincidunt
                        euismod.
                        Nullam euismod tortor sed lectus facilisis, ac suscipit urna consectetur. Praesent eget turpis nec
                        justo
                        varius
                        malesuada
                    </p>
                    <div class="btn">
                        <button class="button-primary">
                            <div class="done">Register</div>
                        </button>
                        <div class="eh">
                            <a href="{{ route('article.edit', ['ID_Kegiatan' => $kegiatan->ID_Kegiatan]) }}"
                                class="img-btn"><img src="https://cdn-icons-png.flaticon.com/128/1159/1159633.png"
                                    alt="edit"></a>
                            <button type="button" onclick="openDeleteModal()" class="img-btn1"><img src="https://i.ibb.co/qdDrY6g/sampah.png" alt="sampah"/></button>

                        </div>
                    </div>
                    {{-- <div class="modal-dialog modal-dialog-centered">

                        <div id="deleteModal" class="modal">
                            <span class="close" onclick="closeDeleteModal()">&times;</span>
                            <h3>Apakah Anda yakin ingin menghapus artikel ini?</h3>
                            <form action="{{ route('article.destroy', ['ID_Kegiatan' => $kegiatan->ID_Kegiatan]) }}"
                                method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit">
                                    <div class="done">Hapus</div>
                                </button>
                            </form>
                        </div>
                    </div> --}}

                    <div class="modal" id="deleteModal" tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Confirmation</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closeDeleteModal()">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p>Are you sure you want to delete this article?</p>
                                </div>
                                <div class="modal-footer">
                                    <form action="{{ route('article.destroy', ['ID_Kegiatan' => $kegiatan->ID_Kegiatan]) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="closeDeleteModal()">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <script src="{{ asset('js/article.js') }}"></script>
    </body>
@endsection

</html>
