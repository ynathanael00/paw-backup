@extends('layouts.main')
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('css/index.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" />
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <style>
        body::-webkit-scrollbar {
            width: 0;
            /* This hides the scrollbar */
        }

        /* For Firefox */
        /* This will hide the scrollbar but keep the scrolling functionality */
        body {
            scrollbar-width: none;
        }

        .welcome-wrapper {
            display: flex;
            justify-content: space-between;
            align-items: center;
            flex-wrap: wrap;
        }

        .text-content {
            flex: 1;
            text-align: left;
        }

        .image-content {
            flex: 1;
            text-align: right;
        }

        .image-content img {
            max-width: 100%;
            height: auto;
            border-radius: 10px;
        }

        .welcome {
            color: #263238;
            text-align: center;
            padding: 40px;
            border-radius: 10px;
            margin-bottom: 20px;
        }

        .welcome h1 {
            color: #263238;
        }

        .welcome p {
            color: #4CAF4F;
            font-weight: bold;
            margin-bottom: 20px;
        }

        .highlight {
            font-weight: bold;
        }

        .know-more {
            text-align: center;
        }

        .explore-button {
            background-color: #4CAF4F;
            color: #fff;
            padding: 10px 20px;
            text-decoration: none;
            font-weight: bold;
            font-size: 16px;
            border-radius: 5px;
            transition: background-color 0.3s ease-in-out;
        }

        .explore-button:hover {
            background-color: #45a049;
        }
    </style>
</head>
@section('container')

    <body style="color: rgba(245, 247, 250, 1)">
        <section class="welcome">
            <div class="container">
                <div class="welcome-wrapper">
                    <div class="text-content">
                        <h1>Welcome to <span class="highlight">Brawevent</span>!</h1>
                        <p>Lorem ipsum lorem ipsum lorem</p>
                        <div class="know-more-wrapper">
                            <a href="#" class="explore-button">Know More</a>
                        </div>
                    </div>
                    <div class="image-content">
                        <img src="https://i.ibb.co/1s3s4WH/Illustration.png" alt="Brawevent Image">
                    </div>
                </div>
            </div>
        </section>

        <div class="container">
            <section>
                <div class="text2">
                    <div class="info">
                        <h3 style="color: green">INFORMATION</h3>
                    </div>
                    <p class="div-info">Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem saepe laborum
                        odit
                        officia consequatur aperiam optio iure libero omnis quos accusantium, iusto, maxime est
                        doloremque! Iure neque quis vel eum quod laboriosam perspiciatis! Dolorum consequuntur officiis,
                        pariatur doloremque sequi dignissimos.</p>
                </div>
            </section>

            <section style="display: flex; justify-content:center">
                <div class="row frame">
                    @foreach ($kegiatans as $kegiatan)
                        <div class="col-md-4 element">
                            <img class="image img-fluid" src="{{ $kegiatan->Foto_Kegiatan }}" />
                            <div class="content">
                                <p class="text-wrapper">{{ $kegiatan->Nama_Kegiatan }}</p>
                                <div class="div">
                                    <div class="text-wrapper-2"><a
                                            href="{{ route('article.show', ['ID_Kegiatan' => $kegiatan->ID_Kegiatan]) }}">Read
                                            more</a></div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </section>
            <section class="buttonAdd">
                <button><a href="{{ route('article.create') }}">Tambah Artikel</a></button>
            </section>
        </div>
    </body>
@endsection

</html>
