<html lang="id">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Update Artikel</title>
    <link rel="stylesheet" href="{{ asset('css/addArticle.css') }}">
</head>
<body>
    <section class="tambah">
        <form action="{{ route('article.update', ['ID_Kegiatan' => $kegiatan->ID_Kegiatan]) }}" method="POST">
            @csrf
            @method('PUT')
            <h1>Update Artikel</h1> 
            <p>Nama Kegiatan</p>
            <input type="text" name="Nama_Kegiatan" value="{{ $kegiatan->Nama_Kegiatan }}"><br>
            <p>Kategori Kegiatan</p>
            <input type="text" name="Kategori_Kegiatan" value="{{ $kegiatan->Kategori_Kegiatan }}"><br>
            <p>Foto Kegiatan</p>
            <input type="text" name="Foto_Kegiatan" value="{{ $kegiatan->Foto_Kegiatan }}"><br>
            <p>Tanggal</p>
            <input type="date" name="Tanggal_Kegiatan" value="{{ $kegiatan->Tanggal_Kegiatan }}"><br>
            <p>Link Pendaftaran</p>
            <input type="text" name="Link_Kegiatan" value="{{ $kegiatan->Link_Kegiatan }}"><br>

            <button type="submit">Update</button>
        </form>
    </section>
</body>
</html>
