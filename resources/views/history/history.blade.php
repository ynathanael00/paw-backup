@extends('layouts.main')

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   
    <link rel="stylesheet" href="historystyle.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        body {
            font-family: sans-serif;
            background-color: #ffffff;
            color: #263238;
            font-size: 16px;

        }

        body::-webkit-scrollbar {
            width: 0;
            /* This hides the scrollbar */
        }

        /* For Firefox */
        /* This will hide the scrollbar but keep the scrolling functionality */
        body {
            scrollbar-width: none;
        }




        .button-container {
            text-align: center;
            margin-top: 10px;
        }

        .button-all,
        .button-lomba,
        .button-oprec,
        .button-webinar {
            background-color: #4CAF4F;
            color: white;
            padding: 0.5rem 1rem;
            border-radius: 5px;
            margin: 0 0.5rem;
        }

        main {
            padding: 1rem;
        }

        section {
            margin-bottom: 1rem;
        }

        h2 {
            font-size: 1.5rem;
        }

        ul {
            list-style-type: none;
            padding: 0;
        }

        li {
            display: flex;
            align-items: center;
        }

        .image-container {
            display: flex;
            border-radius: 5px;
            padding: 1rem;
            margin-right: 1rem;
            align-items: center;
            flex-wrap: wrap;

        }

        .image-container img {
            height: 100px;
            width: 100%;
            border-radius: 5px;
        }

        .lomba-info {
            flex-grow: 1;
        }

        button-done,
        button:hover {
            background-color: #4CAF4F;
            color: white;
            padding: 0.5rem 1rem;
            border-radius: 5px;
        }

        .button-done {
            background: #d9d9d9b5;
            padding: 0.5rem 1rem;
            border-radius: 5px;
        }

        .handsome {
            margin-left: -1em;
            padding: 0.7em;
            border-radius: 10px;
        }

        footer {
            background-color: #263238;
            color: #fff;
            text-align: center;
            padding: 10px;
            bottom: 0;
            width: 100%;
        }

        footer p {
            display: inline;
            margin: 0 10px;
        }
    </style>
</head>

<body>
    @section('container')
        <div class="container-2" style="display: flex; flex-direction:column;">
            <div class="button-container" style="display:flex; justify-content:center">
                <form action="/history/all"><input type="hidden"><button
                        class="button-all">All</button></form>
                <form action="/history"><input type="hidden" name="category" value="Oprec"><button
                        class="button-oprec">Oprec</button></form>
                <form action="/history"><input type="hidden" name="category" value="Lomba"><button
                        class="button-lomba">Lomba</button></form>
                <form action="/history"><input type="hidden" name="category" value="Seminar"><button
                        class="button-webinar">Seminar</button></form>
            </div>
            <main style="">
                <section id="lomba" class="lomba-mewarnai">
                    <ul>
                        @if ($category !== null)
                            <li>
                                <h2>{{ $category }}</h2>
                            </li>
                        @else
                            <li>
                                <h2>All</h2>
                            </li>
                        @endif
                        @if ($history->isEmpty())
                            <h2 style="color: gray">Belum ada kegiatan</h2>
                        @endif
                    </ul>
                    @foreach ($history as $hist)
                        <ul>
                            <li class="handsome"
                                style="width: 100%; {{ $hist->Status_Kegiatan == 'Completed' ? 'background-color: #4CAF4F;' : 'background-color: #D9D9D9;' }}">
                                <div style="display: flex; width:100%;">
                                    <div class="image-container">
                                        <img src="{{ $hist->kegiatan->Foto_Kegiatan }}" alt="Lomba" style="">
                                    </div>
                                    <div class="lomba-info" style="display: flex; align-items:center; width:fit-content;">
                                        <div>
                                            <h3>{{ $hist->kegiatan->Nama_Kegiatan }}</h3>
                                            <h4>{{ $hist->Status_Kegiatan }}</h4>
                                        </div>

                                        {{-- <div class="ms-auto">
                                            <p style="">{{ $hist->updated_at }}</p>
                                        </div> --}}
                                        <form class="ms-auto px-4"
                                            action="{{ route('History.edit', ['id_history' => $hist->ID_History]) }}"
                                            method="GET"
                                            style="{{ $hist->Status_Kegiatan == 'Completed' ? 'display: none;' : '' }}">
                                            @if (!Request::is('history/all'))
                                                <input type="hidden" name="category"
                                                    value="{{ $hist->kegiatan->Kategori_Kegiatan }}">
                                            @else
                                                <input type="hidden" name="category" value="All">
                                            @endif
                                            <button class="button-done" type="submit">Done</button>
                                            
                                        </form>



                                    </div>
                                </div>
                            </li>
                        </ul>
                    @endforeach
                </section>
            </main>
        </div>
    </body>
@endsection

</html>
