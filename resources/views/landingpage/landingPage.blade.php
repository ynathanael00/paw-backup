@extends('layouts.main')
<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <link rel="stylesheet" href="lpstyle.css"> -->

    <style>
        body {
            
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
}

header {
    background-color: #263238;
    padding: 20px;
    border-bottom: 1px solid #ddd;
}


.logo {
    width: 150px;
    height: auto;
}

.welcome-wrapper {
    display: flex;
    justify-content: space-between;
    align-items: center;
    flex-wrap: wrap;
}

.text-content {
    flex: 1;
    text-align: left;
}

.image-content {
    flex: 1;
    text-align: right;
}

.image-content img {
    max-width: 100%;
    height: auto;
    border-radius: 10px;
}

.welcome {
    color: #263238; 
    text-align: center;
    padding: 40px;
    border-radius: 10px;
    margin-bottom: 20px;
}

.welcome h1 {
    color: #263238;
}

.welcome p {
    color: #4CAF4F; 
    font-weight: bold; 
    margin-bottom: 20px; 
}

.highlight {
    font-weight: bold;
}

.know-more {
    text-align: center;
}

.explore-button {
    background-color: #4CAF4F;
    color: #fff;
    padding: 10px 20px;
    text-decoration: none;
    font-weight: bold;
    font-size: 16px;
    border-radius: 5px;
    transition: background-color 0.3s ease-in-out;
}

.explore-button:hover {
    background-color: #45a049;
}

footer {
    background-color: #263238;
    color: #fff;
    text-align: center;
    padding: 10px;
    position: fixed;
    bottom: 0;
    width: 100%;
}

footer p {
    display: inline;
    margin: 0 10px;
}

    </style>
</head>

@section('container')
<body>
        <section class="welcome">
            <div class="container">
                <div class="welcome-wrapper">
                    <div class="text-content">
                        <h1>Welcome to <span class="highlight">Brawevent</span>!</h1>
                        <p>Lorem ipsum lorem ipsum lorem</p>
                        <div class="know-more-wrapper">
                            <a href="#" class="explore-button">Know More</a>
                        </div>
                    </div>
                    <div class="image-content">
                        <img src="https://i.ibb.co/1s3s4WH/Illustration.png" alt="Brawevent Image">
                    </div>
                </div>
            </div>
        </section>
</body>
@endsection
</html>
