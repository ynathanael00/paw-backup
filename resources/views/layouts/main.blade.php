<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brawevent | {{ $title }}</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/navbar-style.css') }}">
</head>

<body class="d-flex flex-column min-vh-100" style="justify-content: space-between">
    <nav class="navbar navbar-expand-lg navbar-clr">
        <div class="container-fluid px-4">
            <a class="navbar-brand text-white" href="/" style="padding-top: 0px;"><img
                    src="https://i.ibb.co/vZHS7ss/Logo-Brawevent.png" alt=""
                    class="img-fluid logo"></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation"> 
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item align-self-center">
                        <a class="nav-link text-white {{ Request::is('home*') ? 'inuse' : '' }}"
                            aria-current="page" href="/home">Home</a>
                    </li>
                    <li class="nav-item align-self-center">
                        <a class="nav-link text-white active {{ Request::is('history*') ? 'inuse' : '' }}"
                            href="/history/all">History</a>
                    </li>
                    <li class="nav-item align-self-center">
                        <a class="nav-link text-white {{ Request::is('sertifikat*') ? 'inuse' : '' }}"
                            href="/sertifikat">Certificate</a>
                    </li>
                    @auth
                        <li class="nav-item align-self-center" style="">
                            <a class="nav-link mx-2 text-white profile-pic" href="#" data-bs-toggle="offcanvas"
                                data-bs-target="#profileSidebar" style="width:50px;">
                                <img src="{{ auth()->user()->Foto_Profile }}"
                                    alt="" class="rounded-circle profile"></a>
                        </li>
                    @else
                    <li class="nav-item align-self-center" style="">
                      <a class="d-flex nav-link text-white" href="/login"
                          style=""><i class="bi bi-indent"></i>Login</a>
                  </li>
                    @endauth

                </ul>
            </div>
        </div>
    </nav>

    {{-- Sidebar --}}
    <div class="offcanvas offcanvas-end" tabindex="-1" id="profileSidebar" aria-labelledby="profileSidebarLabel">
        <div class="offcanvas-header">
            <button type="button" class="btn-close ms-auto text-reset" data-bs-dismiss="offcanvas"
                aria-label="Close"></button>
        </div>
        <div class="offcanvas-body">
            <!-- Profile Information -->
            @auth
            <img src="{{ auth()->user()->Foto_Profile }}"
                alt="Profile Picture" class="rounded-circle off-profile">
            
                <ul class="nav flex-column" style="width: 100%;">
                    <li class="nav-item align-self-center">
                        <h4>{{ auth()->user()->NIM }}</h4>
                    </li>
                    <li class="nav-item align-self-center mb-4">
                        <h4>{{ auth()->user()->Nama_User }}</h4>
                    </li>
                    <li class="nav-item align-self-center my-1"> 
                      <a class="nav-link text-black" href="/history" style="font-size: large">History</a></li>
                    <li class="nav-item align-self-center my-1"> 
                      <a class="nav-link text-black" href="/sertifikat" style="font-size: large" >Certificate</a></li>
                    <li class="nav-item align-self-center my-1"> 
                      <a class="nav-link text-black" href="/profiles"  style="font-size: large">Profile</a></li>
                    <li class="nav-item align-self-center my-1"> 
                      <a class="nav-link text-black" href="#"  style="font-size: large">Settings</a></li>
                    <li class="nav-item align-self-center my-1">
                        <form action="/logout" method="post">
                            @csrf
                            <button class="nav-link text-black"  style="font-size: large">Logout</button>
                        </form>
                    </li>
                </ul>
            @else
            <img src="https://i.ibb.co/dPXsc4G/Vector-2.png"
            alt="Profile Picture" class="rounded-circle off-profile">
                <ul class="nav flex-column">
                    <li class="nav-item align-self-center">
                        <h5>Profile</h5>
                    </li>
                    <li class="nav-item align-self-center">
                        <form action="/login" method="GET"> 
                          @csrf
                          <button class="nav-link text-black">Login</button></form>
                    </li>
                </ul>
            @endauth

            <!-- Add more profile information here -->
        </div>
    </div>

    <div style="min-height:80vh; display:flex; flex-direction:column">
        @yield('container')
    </div>

    <footer class="">
        <div>
            <p>Copyright © 2023 Brawevent Ltd.</p>
            <p>All rights reserved</p>
            <p>Support: support@brawevent.com</p>
        </div>
    </footer>
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">
</script>

</html>
