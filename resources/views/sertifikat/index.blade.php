@extends('layouts.main')
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/sertifikat-style.css') }}">
    <style>
        body::-webkit-scrollbar {
            width: 0;
            /* This hides the scrollbar */
        }

        /* For Firefox */
        /* This will hide the scrollbar but keep the scrolling functionality */
        body {
            scrollbar-width: none;
        }
    </style>
</head>

@section('container')

    <body>

        @if ($sertifikat->isEmpty())
            <div class="d-flex justify-content-center mt-4" style="">
                <h1 style="color: gray">Belum ada sertifikat</h1>
            </div>
        @else
            @foreach ($sertifikat as $yo)
                <div class="card card-bg">
                    <div class="row g-0">
                        <div class="col-1">
                        </div>
                        <div class="col-md-4 p-4 cont-foto">
                            <img src="{{ $yo->Gambar_Sertifikat }}" class="img-fluid rounded foto-sertifikat"
                                alt="...">
                        </div>
                        <div class="col-4 sekat-ajah" style="width: 5em">

                        </div>
                        <div class="col-md-4 p-4 m-2">
                            <div class="card-body text-size">
                                <h5 class="card-title">{{ $yo->kegiatan->Nama_Kegiatan }}</h5>
                                <p class="card-text">{{ $yo->Deskripsi_Sertifikat }}</p>
                                <p style="color: red">{{ $yo->Tanggal_Kegiatan }}</p>
                                <a href="{{ $yo->Gambar_Sertifikat }}"
                                    download="sertifikat{{ $yo->ID_Sertifikat }}.pdf"><button type="button"
                                        class="btn btn-success mt-3">Download</button></a>
                            </div>
                        </div>
                    </div>
                </div>

                <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
                    integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">
                </script>
            @endforeach
        @endif
    @endsection

</html>
