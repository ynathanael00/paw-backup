<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Kegiatan;
use App\Models\User;
use App\Models\Sertifikat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class certController extends Controller
{
    public function index()
    {
        $sertifikat = Sertifikat::all();
        $title = 'Sertifikat';
        return view('sertifikat.index', compact('sertifikat', 'title'));
    }

    public function getUserCert() {
            $title = 'Sertifikat';
            $NIM = auth()->user()->NIM;
            $profile = User::find($NIM);
    
            $sertifikat = $profile->sertifikat;
            return view('sertifikat.index', compact('sertifikat', 'title'));
    }
}
