<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Home;

class ArticleController extends Controller
{
    public function show($idKegiatan)
    {
        $kegiatan = Home::find($idKegiatan);
        $title = 'Artikel';
        return view('home.article', ['kegiatan' => $kegiatan, 'title' => $title]);
    }

    public function edit($ID_Kegiatan)
    {
        $kegiatan = Home::find($ID_Kegiatan);
        return view('home.editArticle', ['kegiatan' => $kegiatan]);
    }

    public function update(Request $request, $ID_Kegiatan)
    {
        $request->validate([
            'Nama_Kegiatan' => 'required',
            'Kategori_Kegiatan' => 'required',
            'Foto_Kegiatan' => 'required|url',
            'Tanggal_Kegiatan' => 'required|date',
            'Link_Kegiatan' => 'required',
        ]);

        $article = Home::find($ID_Kegiatan);
        $article->update($request->all());
        return redirect()->route('index')->with('success', 'Artikel berhasil diperbarui');
    }

    public function destroy($ID_Kegiatan)
    {
        $article = Home::find($ID_Kegiatan);
        $article->delete();

        return redirect()->route('index')->with('success', 'Artikel berhasil dihapus');
    }
}
