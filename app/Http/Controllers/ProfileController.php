<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function update(Request $request)
    {
        $user = auth()->user();
        $user->Nama_User = $request->input('nama');
        $user->NIM = $request->input('nim');
        $user->Fakultas = $request->input('fakultas');
        $user->Program_Studi = $request->input('program_studi');
        $user->save();
    
        return response()->json(['message' => 'Profile updated successfully']);
    }
}
