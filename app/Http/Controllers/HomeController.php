<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Home;

class HomeController extends Controller
{
    public function index()
    {
        $kegiatans = Home::all();
        $title = "Home";
        return view('home.index', compact('kegiatans', 'title'));
    }

    
    public function create()
    {
        return view('home.create');
    }

    
    public function store(Request $request)
    {
        $request->validate([
            'Kategori_Kegiatan' => 'required',
            'Nama_Kegiatan' => 'required',
            'Foto_Kegiatan' => 'required',
            'Tanggal_Kegiatan' => 'required|date',
            'Link_Kegiatan' => 'required',
        ]);

        Home::create($request->all());

        return redirect()->route('home.index')
            ->with('success', 'Kegiatan created successfully');
    }

    public function show(Home $home)
    {
        return view('home.show', compact('home'));
    }

}
