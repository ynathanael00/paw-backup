<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Home;

class AddArticleController extends Controller
{
    public function create() 
    {
        return view('home.addArticle');    
    }

    public function store(Request $request)
    {
        $request->validate([
            'Nama_Kegiatan' => 'required',
            'Kategori_Kegiatan' => 'required',
            'Foto_Kegiatan' => 'required|url',
            'Tanggal_Kegiatan' => 'required|date',
            'Link_Kegiatan' => 'required',
        ]);

        Home::create($request->all());

        return redirect()->route('index')->with('success', 'Artikel berhasil ditambahkan');

    }
}
