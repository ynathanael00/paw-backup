<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\History;
use App\Models\User;
use App\Models\Kegiatan;

class HistoryController extends Controller
{
    public function index(){
        $NIM = auth()->user()->NIM;
        $profile = User::find($NIM);
        $history = $profile->history;
        $category = "All";  
        $title = 'History';
        return view('history.history', compact('history', 'category', 'title'));
    }

    public function show(Request $request){
        $NIM = auth()->user()->NIM;
        $profile = User::find($NIM);
        $history = $profile->history;
        
        $title = 'History';
        $category = $request->input('category');
    
        // Query the History model based on the category, or get all if no category is specified
        if ($category) {
            $history = $history->filter(function ($item) use ($category) {
                return optional($item->kegiatan)->Kategori_Kegiatan === $category;
            });
        }
    
        // Pass the filtered history to the view
        return view('history.history', compact('history', 'category', 'title'));
    }

    public function edit(Request $request, $id_history) {
        try {
            $history = History::where('ID_History', $id_history)->firstOrFail();
            // Perbarui kolom Status_Kegiatan menjadi "complete"
            $history->update(['Status_Kegiatan' => 'Completed']);
            if($request->input('category') == 'All'){
                return $this->index();
            }
            return $this->show($request);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Gagal memperbarui status', 'message' => $e->getMessage()], 500);
            
        }
    }
   

}
