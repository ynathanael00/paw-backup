<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NamaController extends Controller
{
    public function show($nim = null)
    {
        // If NIM is not provided, get the first profile in the database
        $profile = $nim ? Profile::find($nim) : Profile::first();
        $title = 'Profile';
        if (!$profile) {
            // Handle the case where no profile is found
            abort(404, 'Profile not found');
        }

        return view('profiles.index', compact('profile', 'title'));
    }
}
