<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Profile extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    protected $table = 'tb_profile';
    protected $primaryKey = 'email';
    
    protected $fillable = [
        'Nama_User', 'Fakultas', 'Program_Studi', 'Username', 'Password'
    ]; 

    protected $hidden = [
        'password',
    ];

    public function sertifikat() {
        return $this->hasMany(Sertifikat::class, 'NIM');
    }

    public function kegiatan() {
        return $this->hasMany(Kegiatan::class, 'NIM');
    }
}
