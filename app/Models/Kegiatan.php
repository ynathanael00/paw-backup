<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kegiatan extends Model
{

    use HasFactory;
    protected $table = 'tb_kegiatan';

    protected $primaryKey = 'ID_Kegiatan';

    protected $fillable = [
        'Kategori_Kegiatan', 'Nama_Kegiatan', 'Foto_Kegiatan', 'Tanggal_Kegiatan', 'Link_Kegiatan'
    ]; 


    public function sertifikat() {
        return $this->hasMany(Sertifikat::class, 'ID_kegiatan');
    }

    public function history() {
        return $this->hasMany(History::class, 'ID_kegiatan');
    }

}
