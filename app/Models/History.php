<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table = 'tb_history';
    protected $primaryKey = 'ID_History';

    protected $fillable = [
        'ID_Kegiatan', 'Status_Kegiatan', 'Nama_Kegiatan'
    ];

    public function kegiatan(){
        return $this->belongsTo(Kegiatan::class, 'ID_Kegiatan');
    }

    public function profile () {
        return $this->belongsTo(User::class, 'NIM');
    }

}
