<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sertifikat extends Model
{

    use HasFactory;
    protected $table = 'tb_sertifikat';
    protected $primaryKey = 'ID_Sertifikat';

    protected $fillable = [
        'Nama_Kegiatan', 'Deskripsi_Sertifikat', 'Tanggal_Kegiatan', 'Gambar_Sertifikat'
    ]; 

    public function kegiatan() {
        return $this->belongsTo(Kegiatan::class, 'id_kegiatan');
    }

    public function profile () {
        return $this->belongsTo(User::class, 'NIM');
    }
}
