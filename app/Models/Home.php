<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    protected $table = 'tb_Kegiatan';
    protected $primaryKey = 'ID_Kegiatan';
    public $timestamps = false; 

    protected $fillable = [
        'Kategori_Kegiatan',
        'Nama_Kegiatan',
        'Foto_Kegiatan',
        'Tanggal_Kegiatan',
        'Link_Kegiatan',
    ];
}
